package com.intopt.ui.userdetail

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import com.intopt.R
import com.intopt.data.model.User
import com.intopt.data.repository.ImageLoadListener
import com.intopt.databinding.ActivityUserDetailBinding
import com.intopt.ui.home.HomeActivity
import com.intopt.ui.home.HomeViewModel
import com.intopt.ui.home.HomeViewModelFactory
import kotlinx.android.synthetic.main.activity_user_detail.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class UserDetailActivity : AppCompatActivity(), KodeinAware, ImageLoadListener {

    companion object {
        val TAG = "UserDetailActivity"
    }

    override val kodein by kodein()
    private val factory: HomeViewModelFactory by instance()

    private var viewModel: HomeViewModel? = null
    private var userData: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userData = intent.getParcelableExtra("data")
        var binding: ActivityUserDetailBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_user_detail)

        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        binding.viewmodel = viewModel

        viewModel?.imageListener = this

        viewModel?.userId = userData?.id.toString()
        viewModel?.userName = userData?.name.toString()
        viewModel?.userLatLang = "Latitude : ${userData?.address?.geo?.lat.toString()}, " +
                "Longitude : ${userData?.address?.geo?.lng.toString()}"

    }

    override fun onImageLoadSuccess(bitmap: MutableLiveData<Bitmap>) {
        bitmap.observeForever {
            Log.e(HomeActivity.TAG, "onSuccess: ${it.width}")
            iv_show.setImageBitmap(it)
        }
    }
}