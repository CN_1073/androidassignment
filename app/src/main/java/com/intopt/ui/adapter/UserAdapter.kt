package com.intopt.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.intopt.R
import com.intopt.data.model.User
import com.intopt.databinding.RawLayoutBinding

class UserAdapter(
    private val userData: List<User>,
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    override fun getItemCount() = userData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UserViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.raw_layout,
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.rawLayoutBinding.user = userData[position]
        holder.rawLayoutBinding.root.setOnClickListener {
            onItemClickListener.onItemClick(userData[position])
        }
    }

    inner class UserViewHolder(
        val rawLayoutBinding: RawLayoutBinding
    ) : RecyclerView.ViewHolder(rawLayoutBinding.root)

    /**
     * This is the interface which is use to get the click of the row
     */
    interface OnItemClickListener {
        fun onItemClick(item: User?)
    }
}