package com.intopt.ui.home

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.intopt.data.repository.HomeRepository
import com.intopt.data.repository.ImageLoadListener
import com.intopt.utils.Coroutines
import com.intopt.utils.IMAGE_URL

/**
 * This is the ViewModel.
 * HomeActivity and UserDetails activity use this viewModel
 * Using ViewModel getting the data from the server API and back to allocate the view
 * using viewModel we also get the Image bitmap from the server
 */
class HomeViewModel(
    private val repository: HomeRepository
) : ViewModel() {

    companion object {
        val TAG = "HomeViewModel"
    }

    val image = MutableLiveData<Bitmap>()
    var homeListener: HomeListner? = null

    // UserDetails Screen
    var imageListener: ImageLoadListener? = null

    var userId: String? = null
    var userName: String? = null
    var userLatLang: String? = null


    /**
     * Click this button to reload the user Data from the server
     * @param view
     */
    fun onReloadData(view: View) {
        getUserData()
    }

    /**
     * Click this showImage button from the userDetails screen its load the image
     * from the image bitmap
     * @param view
     */
    fun onShowImage(view: View) {
        imageListener?.onImageLoadSuccess(image)
    }

    /**
     * API call
     */
    fun getUserData() {
        homeListener?.onStarted()
        Coroutines.main {
            val response = repository.getUserData()
            homeListener?.onSuccess(response)
        }
    }

    /**
     * Using this method we can get the image bitmap from the server URL
     * @param context
     */
    fun getImageBitmap(context: Context) {

        Glide.with(context)
            .asBitmap()
            .load(IMAGE_URL)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    Log.e(TAG, "onResourceReady: ImageReady")
                    image.value = resource
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    // this is called when imageView is cleared on lifecycle call or for
                    // some other reason.
                    // if you are referencing the bitmap somewhere else too other than this imageView
                    // clear it here as you can no longer have the bitmap
                }
            })
    }

}