package com.intopt.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.intopt.R
import com.intopt.data.model.User
import com.intopt.databinding.ActivityHomeBinding
import com.intopt.ui.adapter.UserAdapter
import com.intopt.ui.userdetail.UserDetailActivity
import com.intopt.utils.hide
import com.intopt.utils.show
import com.intopt.utils.toast
import kotlinx.android.synthetic.main.activity_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

/**
 * HomeActivity to show the UserList
 */
class HomeActivity : AppCompatActivity(), HomeListner, KodeinAware,
    UserAdapter.OnItemClickListener {

    override val kodein by kodein()
    private val factory: HomeViewModelFactory by instance()
    private var viewModel: HomeViewModel? = null

    companion object {
        val TAG = "HomeActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var binding: ActivityHomeBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_home)
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        binding.viewmodel = viewModel

        viewModel?.homeListener = this
        viewModel?.getUserData()

    }

    override fun onResume() {
        super.onResume()
        viewModel?.getImageBitmap(this)
    }

    override fun onStarted() {
        progress_bar.show()
    }

    override fun onSuccess(response: LiveData<List<User>>) {
        progress_bar.hide()

        response.observe(this, Observer {
            Log.e(TAG, "onSuccess: ${it.size}")
            initRecyclerView(it)
        })
    }


    private fun initRecyclerView(userList: List<User>) {
        val mAdapter = UserAdapter(userList, this)

        lv_user.apply {
            layoutManager = LinearLayoutManager(this@HomeActivity)
            setHasFixedSize(true)
            adapter = mAdapter
        }
    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        toast(message)
    }

    override fun onItemClick(item: User?) {

        Intent(this, UserDetailActivity::class.java).also {
            it.putExtra("data", item)
            startActivity(it)
        }
    }
}