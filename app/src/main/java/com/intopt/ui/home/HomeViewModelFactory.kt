package com.intopt.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.intopt.data.repository.HomeRepository

class HomeViewModelFactory(
    private val repository: HomeRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {

            val key = "HomeViewModel"
            if (hashMapViewModel.containsKey(key)) {
                return getViewModel(key) as T
            } else {
                addViewModel(key, HomeViewModel(repository))
                return getViewModel(key) as T
            }
        }
        throw IllegalAccessException("Unknown ViewModel Class")
    }

    companion object {
        val hashMapViewModel = HashMap<String, ViewModel>()
        fun addViewModel(key:String, viewModel:ViewModel) {
            hashMapViewModel[key] = viewModel
        }

        fun getViewModel(key:String): ViewModel? {
            return hashMapViewModel[key]
        }
    }
}