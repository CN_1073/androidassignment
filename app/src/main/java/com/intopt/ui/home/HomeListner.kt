package com.intopt.ui.home

import androidx.lifecycle.LiveData
import com.intopt.data.model.User

interface HomeListner {
    fun onStarted()
    fun onSuccess(response: LiveData<List<User>>)
    fun onFailure(message: String)
}