package com.intopt.data.network

import com.intopt.data.model.User
import com.intopt.utils.SERVER_URL
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET

interface MyApi {




    @GET("users")
    suspend fun getUserData(): Response<List<User>>

    companion object {
        operator fun invoke(
            interceptor: NetworkConnectionInterceptor
        ): MyApi {

            val okHttpClient  = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

            return Retrofit.Builder()
                .baseUrl(SERVER_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MyApi::class.java)
        }
    }
}