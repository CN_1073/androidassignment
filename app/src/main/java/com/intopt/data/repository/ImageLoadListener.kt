package com.intopt.data.repository

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData

interface ImageLoadListener {
    fun onImageLoadSuccess(bitmap: MutableLiveData<Bitmap>)
}