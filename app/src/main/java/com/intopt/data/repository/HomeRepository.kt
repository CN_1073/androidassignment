package com.intopt.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.intopt.data.model.User
import com.intopt.data.network.MyApi
import com.intopt.data.network.SafeApiRequest

class HomeRepository(
    val api: MyApi
) : SafeApiRequest() {

    companion object {
        val TAG = "HomeRepository"
    }
    private val userDataList = MutableLiveData<List<User>>()

    suspend fun getUserData(): LiveData<List<User>> {

        val response =  apiRequestList { api.getUserData() }
        Log.e(TAG, "getUserData: ${response.size}")
        userDataList.postValue(response)
        return userDataList
    }
}